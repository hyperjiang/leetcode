package main

import (
	"reflect"
	"testing"
)

func Test_addTwoNumbers(t *testing.T) {
	type args struct {
		l1 *ListNode
		l2 *ListNode
	}
	tests := []struct {
		name string
		args args
		want *ListNode
	}{
		{
			"1",
			args{
				arr2list([]int{2, 4, 3}),
				arr2list([]int{5, 6, 4}),
			},
			arr2list([]int{7, 0, 8}),
		},
		{
			"2",
			args{
				arr2list([]int{5}),
				arr2list([]int{5}),
			},
			arr2list([]int{0, 1}),
		},
		{
			"3",
			args{
				arr2list([]int{5, 1}),
				arr2list([]int{5}),
			},
			arr2list([]int{0, 2}),
		},
		{
			"4",
			args{
				arr2list([]int{5}),
				arr2list([]int{5, 1}),
			},
			arr2list([]int{0, 2}),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := addTwoNumbers(tt.args.l1, tt.args.l2); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("addTwoNumbers() = %v, want %v", got, tt.want)
			}
		})
	}
}
