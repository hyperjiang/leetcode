package main

// ListNode -
type ListNode struct {
	Val  int
	Next *ListNode
}

func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {

	arr1 := list2arr(l1)
	arr2 := list2arr(l2)

	len1 := len(arr1)
	len2 := len(arr2)
	max := len1
	if max < len2 {
		max = len2
	}

	var arr []int
	var carry int
	for i := 0; i < max; i++ {
		var n1, n2 int
		if i > len1-1 {
			n1 = 0
		} else {
			n1 = arr1[i]
		}
		if i > len2-1 {
			n2 = 0
		} else {
			n2 = arr2[i]
		}
		n := n1 + n2 + carry
		if n >= 10 {
			n -= 10
			carry = 1
		} else {
			carry = 0
		}
		arr = append(arr, n)
	}
	if carry > 0 {
		arr = append(arr, carry)
	}

	return arr2list(arr)
}

func list2arr(n *ListNode) []int {
	var arr []int
	for {
		arr = append(arr, n.Val)
		if n.Next == nil {
			break
		}
		n = n.Next
	}
	return arr
}

func arr2list(arr []int) *ListNode {
	var n *ListNode
	for i := len(arr) - 1; i >= 0; i-- {
		n = &ListNode{
			Val:  arr[i],
			Next: n,
		}
	}
	return n
}
