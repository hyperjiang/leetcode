package main

func lengthOfLongestSubstring(s string) int {

	m := make(map[uint8]int, len(s))

	var max, pos int
	for i := 0; i < len(s); i++ {
		if v, ok := m[s[i]]; ok {
			if v+1 > pos {
				pos = v + 1
			}
		}

		m[s[i]] = i

		if i-pos+1 > max {
			max = i - pos + 1
		}
	}

	return max
}
