package main

func twoSum(nums []int, target int) []int {
	var numsMap = make(map[int]int, len(nums))
	for i, n := range nums {
		c := target - n
		if v, ok := numsMap[c]; ok {
			return []int{v, i}
		}
		numsMap[n] = i
	}
	return nil
}
