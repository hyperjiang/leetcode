package main

func findMedianSortedArrays(nums1 []int, nums2 []int) float64 {
	data := mergeNums(nums1, nums2)

	i := len(data) / 2
	if len(data)%2 == 1 { // odd
		return float64(data[i])
	}
	// even
	return float64(data[i-1]+data[i]) / 2

}

func mergeNums(nums1 []int, nums2 []int) []int {

	len1 := len(nums1)
	len2 := len(nums2)

	if len1 == 0 {
		return nums2
	}
	if len2 == 0 {
		return nums1
	}

	res := make([]int, len1+len2)
	var i, j, k int
	for {
		if i+j >= len1+len2 {
			break
		}

		if i >= len1 {
			res[k] = nums2[j]
			j++
			k++
			continue
		}

		if j >= len2 || nums1[i] <= nums2[j] {
			res[k] = nums1[i]
			i++
			k++
		} else {
			res[k] = nums2[j]
			j++
			k++
		}
	}

	return res
}
