package main

import "testing"

func Test_findMedianSortedArrays(t *testing.T) {
	type args struct {
		nums1 []int
		nums2 []int
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{
			"1",
			args{
				[]int{1, 3},
				[]int{2},
			},
			float64(2.0),
		},
		{
			"2",
			args{
				[]int{1, 2},
				[]int{3, 4},
			},
			float64(2.5),
		},
		{
			"3",
			args{
				[]int{1, 3},
				[]int{},
			},
			float64(2.0),
		},
		{
			"4",
			args{
				[]int{},
				[]int{3, 4},
			},
			float64(3.5),
		},
		{
			"5",
			args{
				[]int{4, 5, 6},
				[]int{1, 2, 3, 4},
			},
			float64(4),
		},
		{
			"6",
			args{
				[]int{4, 5, 6},
				[]int{1, 2, 3},
			},
			float64(3.5),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := findMedianSortedArrays(tt.args.nums1, tt.args.nums2); got != tt.want {
				t.Errorf("findMedianSortedArrays() = %v, want %v", got, tt.want)
			}
		})
	}
}
