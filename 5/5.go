package main

// For each index, use it as the middle of a palindrome and keep expanding outwards until it is no longer a palindrome (odd length palindromes).
// Check whether the size of the palindrome is bigger than our best and update if it is.
// Check whether for each index and it's neighbour forms a palindrome, if so, expand outwards the same as point 1.
func longestPalindrome(s string) string {

	if len(s) <= 1 {
		return s
	}

	var pos, max = 0, 1

	for middle := 0; middle < len(s); middle++ {
		// Check the odd length palindromes and then the even length palindromes
		for _, right := range []int{middle, middle + 1} {
			left := middle

			// Keep expanding outwords, so long as we still have a palindrome
			for left >= 0 && right < len(s) && s[left] == s[right] {
				left--
				right++
			}

			// Do we have a better solution? Keep in mind that left and right are now either side
			// of the palindrome, so to get the correct start and length, left must be moved back.
			left++
			len := right - left
			if len > max {
				pos = left
				max = len
			}
		}
	}

	return s[pos : pos+max]
}
