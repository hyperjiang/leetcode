package main

import "testing"

func Test_longestPalindrome(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			"1",
			args{"babad"},
			[]string{"bab", "aba"},
		},
		{
			"2",
			args{"cbbd"},
			[]string{"bb"},
		},
		{
			"3",
			args{"abcda"},
			[]string{"a", "b", "c", "d"},
		},
		{
			"4",
			args{""},
			[]string{""},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := longestPalindrome(tt.args.s)
			ok := false
			for _, w := range tt.want {
				if got == w {
					ok = true
				}
			}

			if !ok {
				t.Errorf("longestPalindrome() = %v, want %v", got, tt.want)
			}
		})
	}
}
